// 1. What directive is used by Node.js in loading the modules it needs?
// require

// 2. What Node.js module contains a method for server creation?
// http

// 3. What is the method of the http object responsible for creating a server using node.js?
// createServer()

// 4. What method of the response object allows us to set status codes and content types?
// writeHead()

// 5. Where will console.log() output its contents when run in Node.js?
// stdout

// 6. What property of the request object contains the address's endpoint?
// url

const http = require("http");

const PORT = 3000;

http
  .createServer(function (request, response) {
    if (request.url == "/login") {
      response.writeHead(200, { "Content-Type": "text/plain" });
      response.end("Welcome to the login page");
    } else {
      response.writeHead(404, { "Content-Type": "text/plain" });
      response.end("Error: Page Not Found");
    }
  })
  .listen(PORT);

console.log(`Server is running at localhost: ${PORT}`);
